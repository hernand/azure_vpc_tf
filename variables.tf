variable "location" {
  description = "A2 || azure location"
  default     = "West US"
}
variable "azurerm_resource_group_name" {
  description = "A1 || azurerm_resource_group_name"
  default     = "production-resources"
}
